import os
import re
import shutil
import gazu
import subprocess, json
import pprint
from kabaret import flow
from datetime import datetime, date


def copytree_no_metadata(src, dst):
    '''
    Recursively copy an entire directory tree rooted at `src`
    to a directory named `dst`, without copying metadata.
    '''
    if not os.path.isdir(dst):
        os.mkdir(dst)
    
    for f in os.scandir(src):
        if f.is_dir():
            copytree_no_metadata(f.path, os.path.join(dst, f.name))
        else:
            shutil.copyfile(f.path, os.path.join(dst, f.name))


class RevisionNameChoiceValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _file = flow.Parent(2)

    def __init__(self, parent, name):
        super(RevisionNameChoiceValue, self).__init__(parent, name)
        self._revision_names = None

    def choices(self):
        if self._revision_names is None:
            self._revision_names = self._file.get_revision_names(sync_status='Available', published_only=True)
        
        return self._revision_names
    
    def revert_to_default(self):
        names = self.choices()
        if names:
            self.set(names[-1])
    
    def touch(self):
        self._revision_names = None
        super(RevisionNameChoiceValue, self).touch()


class Studio30UploadPlayblastToKitsu(flow.Action):

    revision = flow.SessionParam(None, RevisionNameChoiceValue)
    comment = flow.SessionParam('')

    _task_type   = flow.Computed()
    _task_status = flow.Computed()
    _delivery_folder = flow.Computed()

    _file       = flow.Parent()
    _task       = flow.Parent(3)
    _shot       = flow.Parent(5)
    _sequence   = flow.Parent(7)

    def needs_dialog(self):
        self.message.set('<h2>Send preview to Studio 3.0</h2>')
        self.revision.touch()
        self.revision.revert_to_default()
        return True
    
    def get_buttons(self):
        return ['Upload', 'Cancel']

    def get_file(self, file_name, rev=None):
        f = None
        try:
            task = self._shot.tasks['assist_key']
        except flow.exceptions.MappedNameError:
            pass
        else:
            try:
                _file = task.files[file_name]
            except flow.exceptions.MappedNameError:
                pass
            else:
                if rev:
                    f = _file.get_revision(rev)
                else:
                    f = _file.get_head_revision()
                if f is not None and not os.path.exists(f.get_path()):
                    f = None
        
        return f
    
    def compute_child_value(self, child_value):
        config = self.root().project().kitsu_config().kitsu_studio_30
        settings = config.get_task_settings(self._task.name())

        if child_value is self._task_type:
            self._task_type.set(settings.get('target_type'))
        elif child_value is self._task_status:
            self._task_status.set(settings.get('target_status'))
        elif child_value is self._delivery_folder:
            settings = self.root().project().admin.project_settings.export_settings
            self._delivery_folder.set(
                os.path.join(
                    settings.root_folder.get(),
                    datetime.now().strftime('%y%m%d'),
                    f'{self._sequence.name()}_{self._shot.name()}'
                )
            )

    def export_files(self, delivery_folder):
        scene = self.get_file('clean_blend', self.revision.get())
        refs = self.get_file('refs')

        print(f'Send to 3.0 Studio :: exporting {self._sequence.name()} {self._shot.name()} -> {delivery_folder}')
        print(f'Send to 3.0 Studio ::   assist key: {scene.name()}')

        if not os.path.isdir(delivery_folder+'/WIP'):
            os.makedirs(delivery_folder+'/WIP', exist_ok=True)
        
        shutil.copyfile(scene.get_path(), delivery_folder+'/WIP/'+os.path.basename(scene.get_path()))

        print(f'Send to 3.0 Studio ::   movie: {self.revision.get()}')
        os.makedirs(delivery_folder+'/EXPORTS', exist_ok=True)
        mov_path = self._file.get_revision(self.revision.get()).get_path()
        shutil.copyfile(mov_path, delivery_folder+'/EXPORTS/'+os.path.basename(mov_path))
        
        if refs is not None:
            print(f'Send to 3.0 Studio ::   refs: {refs.name()}')
            if os.path.isdir(delivery_folder+'/IMAGES'):
                shutil.rmtree(delivery_folder+'/IMAGES')
            
            copytree_no_metadata(refs.get_path(), delivery_folder+'/IMAGES')
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        if self._sequence.name() not in self.root().project().admin.project_settings.export_settings.skip_kitsu_30_update.get():
            store_lfs_user = gazu.client.default_client.tokens

            # Switch to 3.0 Studio Kitsu
            config = self.root().project().kitsu_config().kitsu_studio_30
            gazu.set_host(config.host.get())
            gazu.log_in(config.login.get(), config.password.get())

            site_env = self.root().project().get_current_site().site_environment
            if site_env.has_mapped_name('FFPROBE_EXEC_PATH'):
                exec_path = site_env['FFPROBE_EXEC_PATH'].value.get()

                check_frames = subprocess.check_output(
                    f'{exec_path} -v quiet -show_streams -select_streams v:0 -of json "{self._file.get_revision(self.revision.get()).get_path()}"',
                    shell=True).decode()

                fields = json.loads(check_frames)['streams'][0]
                frames = int(fields['nb_frames'])

                sequence_data = gazu.shot.get_sequence_by_name(config.get_project_id(), self._sequence.name())
                shot_data = gazu.shot.get_shot_by_name(sequence_data, self._shot.name())

                if frames < shot_data['nb_frames']:
                    gazu.set_host(self.root().project().kitsu_config().server_url.get()+'/api')
                    gazu.client.set_tokens(store_lfs_user)

                    self.message.set(f'<font color=red><b>Revision has {shot_data["nb_frames"]-frames} missing frames</font></b>')
                    return self.get_result(close=False)

            config.upload_shot_preview(
                self._sequence.name(),
                self._shot.name(),
                self._task_type.get(),
                self._task_status.get(),
                self._file.get_revision(self.revision.get()).get_path(),
                self.comment.get()
            )

            # Go back to LFS Kitsu and restore user token
            gazu.set_host(self.root().project().kitsu_config().server_url.get()+'/api')
            gazu.client.set_tokens(store_lfs_user)
        else:
            print(f'Send to 3.0 Studio :: skip upload to 3.0 Kitsu ({self._sequence.name()})')
            print('Send to 3.0 Studio :: compress movie')
            
            # Convert movies to MP4
            mov_path = self._file.get_revision(self.revision.get()).get_path()
            mp4_path = os.path.splitext(mov_path)[0] + '.mp4'
            ffmpeg_path = self.root().project().admin.project_settings.ffmpeg_path.get()
            subprocess.run([
                ffmpeg_path,
                '-loglevel',
                'panic',
                '-y',
                '-i',
                mov_path,
                '-vcodec',
                'libx264',
                '-crf',
                '28',
                mp4_path
            ])
            print(f'Send to 3.0 Studio ::   movie: {mp4_path}')

        self.root().project().kitsu_api().set_shot_task_status(
            self._sequence.name(),
            self._shot.name(),
            'assist_key',
            'WFA_3.0',
            f'Envoyé le {date.today().strftime("%d/%m/%y")} à 3.0'
        )

        self.export_files(self._delivery_folder.get())


class Studio30Synchronize(flow.Action):

    ICON = ('icons.libreflow', 'kitsu')

    def __init__(self, parent, name):
        super(Studio30Synchronize, self).__init__(parent, name)
        self._previews = {}
        self._previews_downloaded = {}
        # How we structure the data
        # Sequences
            # Shots
                # Tasks
                    # Preview

    def needs_dialog(self):
        return False

    def get_previews(self, config, sequences, task_sync):
        # Get all sequences
        sequences_data = [
            gazu.shot.get_sequence_by_name(config.get_project_id(), name)
            for name in sequences
        ]

        # Get shots
        for seq in sequences_data:
            shots_data = gazu.shot.all_shots_for_sequence(seq)

            if shots_data:
                self._previews[seq['name']] = {}

                # Get previews
                for shot in shots_data:                  
                    previews_data = gazu.shot.all_previews_for_shot(shot)
                    if not previews_data:
                        continue
                    
                    self._previews[seq['name']][shot['name']] = {}

                    # Get latest preview of tasks we want
                    for task_name, task_id in task_sync.items():
                        # Exception for ASSIST-Key task
                        if task_name == "ASSIST-Key":
                            shot_task_data = self.root().project().kitsu_api().get_task(shot, task_name)
                            if shot_task_data['task_status']['name'] not in ('DONE', '(DONE)'):
                                continue

                        # Keep only ones from task
                        previews_from_task = {k:v for k, v in previews_data.items() if k == task_id}
                        if not previews_from_task:
                            continue

                        previews_from_task = previews_from_task[task_id]

                        # Get the latest
                        if len(previews_from_task) > 1:
                            latest_preview = max(previews_from_task, key=lambda p: p["revision"])
                        else:
                            latest_preview = previews_from_task[0]
                        
                        self._previews[seq['name']][shot['name']][task_name] = latest_preview
                        print(f"Sync ::   {seq['name']} {shot['name']} {task_name} fetched")

    def download_previews(self):
        film = self.root().project().films.mapped_items()[0]
        task_manager = self.root().project().get_task_manager()

        # Sequences
        for seq in self._previews:
            sequence = film.sequences[seq] if film.sequences.has_mapped_name(seq) else film.sequences.add(seq)
            self._previews_downloaded[seq] = {}
            
            # Shot
            for sh in self._previews[seq]:
                shot = sequence.shots[sh] if sequence.shots.has_mapped_name(sh) else sequence.shots.add(sh)
                task = shot.tasks.mapped_items()[0]
                self._previews_downloaded[seq][sh] = {}

                # Previews
                for t in self._previews[seq][sh]:
                    preview_data = self._previews[seq][sh][t]

                    # Exception for ASSIST-Key task
                    # If there is a working file then ignore
                    if t == 'ASSIST-Key':
                        if task.files.has_file('clean', 'blend'):
                            if task.files['clean_blend'].get_head_revision():
                                continue
                    
                    # Define file name
                    if t == 'ANIMATIQUE':
                        file_name = 'animatic.mov'
                    elif t == 'ANIM-Key':
                        file_name = 'anim_key.mov'
                    elif t == 'ASSIST-Key':
                        file_name = 'assist_key.mov'

                    file_mapped_name = file_name.replace('.', '_')
                    name, ext = os.path.splitext(file_name)
                    ext = ext[1:]

                    # Check file
                    r = None
                    if task.files.has_file(name, ext):
                        f = task.files[file_mapped_name]

                        # Check if revision already downloaded
                        for rev in f.get_revisions().mapped_items():
                            rev_number = int(re.search('(?<=Revision )[^ of]*', rev.comment.get()).group(0))
                            if rev_number == int(preview_data['revision']):
                                r = rev
                                break

                        # Check if revision file exists (in case for download/convert issues)
                        if r:
                            output_path = r.get_path()
                            if os.path.exists(output_path):
                                continue
                        
                    else:
                        default_files = task_manager.get_task_files(task.name())
                        default_file = default_files.get(file_mapped_name)

                        f = task.files.add_file(
                            name,
                            ext,
                            tracked=True,
                            default_path_format=default_file[1]
                        )
                        f.file_type.set('Inputs')

                    # Add revision
                    if r is None:
                        r = f.add_revision(
                            comment=f"From Revision {preview_data['revision']} of 3.0 Kitsu",
                            path_format=f"{os.path.dirname(f.path_format.get())}/{preview_data['original_name']}"
                        )

                    # Output variables
                    output_path = r.get_path()
                    output_dir = os.path.split(output_path)[0]
                    temp_path = output_path.replace('.mov', '.mp4')

                    # Download preview
                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)
                    gazu.files.download_preview_file(preview_data, temp_path)
                    print(f"Sync ::   {seq} {sh} {t} downloaded")

                    # Convert to MOV
                    ffmpeg_path = self.root().project().admin.project_settings.ffmpeg_path.get()
                    cmd = [
                        ffmpeg_path,
                        '-n',
                        '-i',
                        temp_path,
                        '-qscale',
                        '0',
                        output_path
                    ]

                    subprocess.run(' '.join(cmd), shell=True)
                    os.remove(temp_path)
                    
                    print(f"Sync ::   {seq} {sh} {t} converted")

                    # Added to downloaded dict
                    self._previews_downloaded[seq][sh][t] = dict(
                        output_path=output_path,
                        rev_number=preview_data['revision']
                    )

    def upload_previews(self):
        # Sequences
        for seq in self._previews_downloaded:
            sequence_data = self.root().project().kitsu_api().get_sequence_data(seq)

            # Shot
            for sh in self._previews_downloaded[seq]:
                shot_data = self.root().project().kitsu_api().get_shot_data(sh, sequence_data)

                # Create shot if not exist
                if shot_data is None:
                    self.root().project().kitsu_api().create_shot(seq, sh, task_status='OMIT')
                
                # Previews
                for t in self._previews_downloaded[seq][sh]:
                    # Get properties
                    output_path = self._previews_downloaded[seq][sh][t]['output_path']
                    rev_number = self._previews_downloaded[seq][sh][t]['rev_number']

                    # Check if revision already uploaded
                    task_data = self.root().project().kitsu_api().get_shot_task(seq, sh, t)
                    comments = self.root().project().kitsu_api().get_all_comments_for_task(task_data)

                    exists = False
                    for rev in comments:
                        remote_rev_number = int(re.search('(?<=Revision )[^ of]*', rev['text']).group(0))
                        if remote_rev_number == rev_number:
                            exists = True
                            break

                    if exists:
                        continue

                    # Upload
                    status = self.root().project().kitsu_api().upload_shot_preview(
                        seq,
                        sh,
                        t,
                        'OMIT',
                        output_path,
                        f"From Revision {rev_number} of 3.0 Kitsu"
                    )

                    if status:
                        print(f"Sync ::   {seq} {sh} {t} uploaded")

    def run(self, button):
        if button == 'Cancel':
            return

        config = self.root().project().kitsu_config().kitsu_studio_30
        task_sync = config.task_sync.get()

        # Get sequences from our Kitsu
        # [:-1] = Ignore test sequence
        sequences_data = self.root().project().kitsu_api().get_sequences_data()
        sequences_names = [seq['name'] for seq in sequences_data if seq][:-1]

        # Switch to 3.0 Studio Kitsu
        print('Sync ::   Connect to 3.0 Kitsu')
        store_lfs_user = gazu.client.default_client.tokens
        gazu.set_host(config.host.get())
        gazu.log_in(config.login.get(), config.password.get())

        self._previews = {}

        print('Sync ::   Fetch previews')
        self.get_previews(config, sequences_names, task_sync)

        print('\nSync ::   Download previews')
        self.download_previews()

        # Go back to LFS Kitsu and restore user token
        print('\nSync ::   Back to LFS Kitsu')
        gazu.set_host(self.root().project().kitsu_config().server_url.get()+'/api')
        gazu.client.set_tokens(store_lfs_user)

        print('Sync ::   Upload previews')
        self.upload_previews()
        print('Sync ::   Action complete')


class Studio30KitsuSettings(flow.Object):

    host = flow.Param().ui(editable=False)
    project_name = flow.Param().ui(editable=False)
    login = flow.Param('')
    password = flow.Param('').ui(editor='password')
    task_settings = flow.Param(dict)
    task_sync = flow.Param().ui(label='Tasks to synchronize')

    _project_id = flow.Computed(cached=True)

    def get_project_id(self):
        return self._project_id.get()
    
    def get_task_settings(self, task_name):
        settings = self.task_settings.get() or {}
        return settings.get(task_name)
    
    def upload_shot_preview(self, sequence_name, shot_name, task_type_name, task_status_name, preview_path, comment):
        sequence = gazu.shot.get_sequence_by_name(self.get_project_id(), sequence_name)
        shot = gazu.shot.get_shot_by_name(sequence, shot_name)
        task_type = gazu.task.get_task_type_by_name(task_type_name)
        task = gazu.task.get_task_by_entity(shot, task_type)
        target_status = gazu.task.get_task_status_by_name(task_status_name)

        print(f'Upload {preview_path} to task {task_type_name} (status: {task_status_name}, comment: \'{comment}\')')

        comment_data = gazu.task.add_comment(task, target_status, comment)
        gazu.task.add_preview(task, comment_data, preview_path)

    def compute_child_value(self, child_value):
        if child_value is self._project_id:
            project_data = gazu.project.get_project_by_name(self.project_name.get()) or {}
            self._project_id.set(project_data.get('id'))


def kitsu_studio_30(parent):
    if re.match('^/march/admin/kitsu$', parent.oid()):
        r = flow.Child(Studio30KitsuSettings).ui(label='3.0 Studio')
        r.name = 'kitsu_studio_30'
        r.index = None
        return r


def upload_playblast_30(parent):
    if re.match('^/march/films/[^/]+/sequences/[^/]+/shots/[^/]+/tasks/[^/]+/files/[^/]+_mov$', parent.oid()):
        r = flow.Child(Studio30UploadPlayblastToKitsu).ui(label='Send to 3.0 Studio')
        r.name = 'upload_playblast_30'
        r.index = None
        return r


def synchronize_30(parent):
    if re.match('^/march/synchronization$', parent.oid()):
        r = flow.Child(Studio30Synchronize).ui(label='Synchronize 3.0 Studio')
        r.name = 'synchronize_30'
        r.index = None
        return r


def install_extensions(session): 
    return {
        "march": [
            kitsu_studio_30,
            upload_playblast_30,
            synchronize_30
        ],
    }
