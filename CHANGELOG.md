# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

### Added

* An action for synchronize shots previews from a custom Kitsu project.
  * It retrieves the latest versions of specific tasks, downloads them and reintegrates them into our kitsu.
* An action to upload a revision of a MOV file to a custom Kitsu project. The configuration of the latter is available in the `admin/kitsu` relation of the project.
